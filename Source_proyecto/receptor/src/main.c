/*==================[inclusions]=============================================*/

#include "main.h"
#include "board.h"

/*==================[macros and definitions]=================================*/

#define RXBUF_SIZE  5
#define PACKAGE_SZ 10
#define TEMP_NEGATIVE 1
#define TEMP_POSITIVE 0
#define MESSAGE_KEY 0xAA
#define DATA_READY 1
#define NOT_READY 0



/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

static void initHardware(void);

/*==================[internal data definition]===============================*/

static uint8_t rx_bytes;
static uint8_t data;
static uint8_t index;

static char buffer[33];

uint8_t rx_buf[3];


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	//	SysTick_Config(SystemCoreClock / 1000);

	/* EDU-CIAA USART2 configuration */
	// P7_1 pin -> U2_TXD @ FUNC6 [UM:Table 190]
	// P7_2 pin -> U2_RXD @ FUNC6
	Chip_SCU_PinMuxSet(2, 3, SCU_MODE_FUNC2);  // [UM:17.4.1]
	Chip_SCU_PinMuxSet(2, 4, SCU_MODE_INBUFF_EN | SCU_MODE_FUNC2);


	//	 USART2 peripheral configuration
	Chip_UART_Init(LPC_USART3);            // 8-N-1 and FIFOs enabled
	Chip_UART_ConfigData(LPC_USART3, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_ODD | UART_LCR_PARITY_DIS ));
	Chip_UART_SetBaud(LPC_USART3, 1200); // [UM:40.6.3]
	//Chip_UART_TXEnable(LPC_USART3);        // [UM:40.6.20]
	Chip_UART_SetupFIFOS(LPC_USART3, UART_FCR_TRG_LEV0 | UART_FCR_FIFO_EN);
	//	Chip_UART_SetupFIFOS(LPC_USART3, UART_FCR_TRG_LEV2 | UART_FCR_FIFO_EN);

	// Enable USART2 RBR interrupt
	Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT);  // [UM:40.6.4]

	//	 Enable USART2 interrupt in the NVIC [UM:9.7]
	NVIC_ClearPendingIRQ(USART3_IRQn);
	NVIC_EnableIRQ(USART3_IRQn);

	Chip_SCU_PinMuxSet(7, 1, SCU_MODE_FUNC6);  // [UM:17.4.1]
	// USART2 peripheral configuration
	Chip_UART_Init(LPC_USART2);            // 8-N-1 and FIFOs enabled
	Chip_UART_SetBaud(LPC_USART2, 115200); // [UM:40.6.3]
	Chip_UART_TXEnable(LPC_USART2);        // [UM:40.6.20]


}


/*==================[external functions definition]==========================*/

void UART3_IRQHandler(void)
{
	if (index == 3)
		{
			index = 0;
		}
	
	rx_bytes = Chip_UART_Read(LPC_USART3, rx_buf + index, 1);
	index++;
		
	

}



static void mostrar_terminal(uint8_t* vector)
{
	uint8_t temp_sign = TEMP_POSITIVE;
		temperatura = vector[1] + (vector[2]<<8);
	//	presion = vector[4] + (vector[5]<<8) + (vector[6]<<16) + (vector[7]<<24);
	//	temperatura = vector[8] + (vector[9]<<8) + (vector[10]<<16) + (vector[11]<<24);

	if (temperatura < 0 )
	{
		temp_sign = TEMP_NEGATIVE;
	}
	Chip_UART_SendBlocking(LPC_USART2, "Temperatura: ", 13);
	itoa((temperatura/100),buffer,10);
	Chip_UART_SendBlocking(LPC_USART2, buffer, (2+temp_sign));
	Chip_UART_SendBlocking(LPC_USART2, ".", 1);
	itoa((abs(temperatura)%100),buffer,10);
	Chip_UART_SendBlocking(LPC_USART2, buffer, 2);
	Chip_UART_SendBlocking(LPC_USART2, "ºC\r\n", 5);

	/*Presion*/
	//	Chip_UART_SendBlocking(LPC_USART2, "Presion: ", 9);
	//	itoa(presion,buffer,10);
	//	Chip_UART_SendBlocking(LPC_USART2, buffer, 6);
	//	Chip_UART_SendBlocking(LPC_USART2, "Pa\r\n", 5);

	/*Humedad*/
	//	Chip_UART_SendBlocking(LPC_USART2, "Humedad: ", 9);
	//	itoa(humedad,buffer,10);
	//	Chip_UART_SendBlocking(LPC_USART2, buffer, 2);
	//	Chip_UART_SendBlocking(LPC_USART2, "%\r\n", 4);
}



int main(void)
{

	initHardware();

	while (1)
	{
			if (rx_buf[0] == MESSAGE_KEY)
			{
				mostrar_terminal(rx_buf);
				rx_buf[0] = 0;
				data = NOT_READY;
			}
		
	}
}

/* USART2 RBR interrupt handler */




/** @} doxygen end group definition */

/*==================[end of file]============================================*/
