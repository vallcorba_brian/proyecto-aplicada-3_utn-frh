/* Copyright 2017, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief This is a simple statechart example using Yakindu Statechart Tool
 * Plug-in (update site: http://updates.yakindu.org/sct/mars/releases/).
 */

/** \addtogroup statechart Simple UML Statechart example.
 ** @{ */

/*==================[inclusions]=============================================*/

#include "main.h"

#include "board.h"


/*==================[macros and definitions]=================================*/
/** GPIO port and pin definitions */
#define TEC1_PORT  0   /* TEC1: P1_0 pin -> GPIO0[4] @ FUNC0 [UM:Table 190] */
#define TEC1_PIN   4

#define LED1_PORT  0   /* LED1: P2_10 pin -> GPIO0[14] @ FUNC0 */
#define LED1_PIN   14
#define LED_ON Chip_GPIO_GetPinState(LPC_GPIO_PORT, LED1_PORT, LED1_PIN)
#define SLAVE_ADD0 0X76

#define PACKAGE_SZ 1
static uint8_t sent_bytes;
static uint32_t time_spent;

union un {
	uint8_t separados[4];
	uint32_t sensor;
};

/** i2c port used */
#ifdef lpc4337_m4
#define I2C_PORT I2C0
#else
#define I2C_PORT I2C1
#endif
/*==================[internal data declaration]==============================*/
uint8_t wbuf[3] = {0,0,0};
uint8_t rbuf[3] = {0,0,0};
I2C_XFER_T xfer;

/** tick flag */
static uint32_t tick;
/** period count*/
static uint32_t count1_ms = 0;
static uint32_t count2_ms = 0;
/** cycle indication flag
 *  0 for ON half-cycle
 *  1 for OFF half-cycle
 */
short int flag = 0;
/** state variable */
short int periodos;


/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** @brief hardware initialization
 *  configuration of clock, GPIO, I2C, interruptions
 *  @return none
 */
static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000);


	Board_I2C_Init(I2C_PORT);
	Chip_I2C_SetClockRate(I2C_PORT, 100000);
	Chip_I2C_SetMasterEventHandler(I2C_PORT, Chip_I2C_EventHandlerPolling);

	/* EDU-CIAA USART2 configuration */
	// P7_1 pin -> U2_TXD @ FUNC6 [UM:Table 190]
	// P7_2 pin -> U2_RXD @ FUNC6
	Chip_SCU_PinMuxSet(2, 3, SCU_MODE_FUNC2);  // [UM:17.4.1]
	//	Chip_SCU_PinMuxSet(2, 4, SCU_MODE_INBUFF_EN | SCU_MODE_FUNC2);


	//	 USART2 peripheral configuration
	Chip_UART_Init(LPC_USART3);            // 8-N-1 and FIFOs enabled
	Chip_UART_SetBaud(LPC_USART3, 1200); // [UM:40.6.3]
	Chip_UART_TXEnable(LPC_USART3);        // [UM:40.6.20]
	Chip_UART_ConfigData(LPC_USART3, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_ODD | UART_LCR_PARITY_DIS ));

	// Enable USART2 RBR and THRE interrupt [UM:40.6.4]
	Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT );

	// Enable USART2 interrupt in the NVIC [UM:9.7]
	NVIC_ClearPendingIRQ(USART3_IRQn);
	NVIC_EnableIRQ(USART3_IRQn);

}

/*==================[external functions definition]==========================*/
/**
 * @brief I2C memory reading function
 * @param addr peripheral addres
 * @param data memory addr to recieve the reading
 * @param len bit length
 * @return transfer status
 */
int16_t memRead (uint16_t addr, void * data, uint16_t len)
{
	xfer.rxBuff = data; //data rbuf ???
	xfer.rxSz = len;	//len 1
	xfer.slaveAddr = addr; //addr 0x50
	xfer.status = 0;
	xfer.txBuff = wbuf;
	xfer.txSz = 2;

	Chip_I2C_MasterTransfer(I2C_PORT, &xfer);

	return xfer.status;
}

int16_t regRead (uint8_t addr, uint8_t reg, void * data, uint16_t len)
{
	uint8_t wbuf[1] = {reg};

	xfer.rxBuff = data; //data rbuf ???
	xfer.rxSz = len;	//len 1
	xfer.slaveAddr = addr; //addr 0x50
	xfer.status = 0;
	xfer.txBuff = wbuf;
	xfer.txSz = 1;

	Chip_I2C_MasterTransfer(I2C_PORT, &xfer);

	return xfer.status;
}

int16_t regWrite (uint8_t addr, uint8_t reg, void * data, uint16_t len)
{
	uint8_t wbuf[1+len];

	wbuf[0] = reg;
	memcpy(wbuf+1, data, len);

	xfer.rxBuff = NULL; //data rbuf ???
	xfer.rxSz = 0;	//len 1
	xfer.slaveAddr = addr; //addr 0x50
	xfer.status = 0;
	xfer.txBuff = wbuf;
	xfer.txSz = 1+len;

	Chip_I2C_MasterTransfer(I2C_PORT, &xfer);

	return xfer.status;
}



/**
 * @brief I2C memory writing function
 * @param addr peripheral addres
 * @param data data to be written
 * @param len bit length
 * @return transfer status
 */
int16_t memWrite (uint16_t addr, void * data, uint16_t len) //memWrite(0x50, wbuf, 3)
{
	xfer.rxBuff = 0;
	xfer.rxSz = 0;
	xfer.slaveAddr = addr; //addr 0x50
	xfer.status = 0;
	xfer.txBuff = data; //data wbuf
	xfer.txSz = len;		//len 3

	Chip_I2C_MasterTransfer(I2C_PORT, &xfer);

	return xfer.status;
}

/** SysTick interrupt handler */
uint32_t pausems_count;
void SysTick_Handler(void)
{
	tick = 1;
	if(pausems_count)
		pausems_count--;
}

static void pausems(uint32_t t)
{
	pausems_count = t;
	while (pausems_count != 0) {
		__WFI();
	}
}

uint16_t dig_T1;
int16_t dig_T2;
int16_t dig_T3;
uint16_t dig_P1;
int16_t dig_P2;
int16_t dig_P3;
int16_t dig_P4;
int16_t dig_P5;
int16_t dig_P6;
int16_t dig_P7;
int16_t dig_P8;
int16_t dig_P9;
uint8_t  dig_H1;
int16_t dig_H2;
uint8_t  dig_H3;
int16_t dig_H4;
int16_t dig_H5;
int8_t  dig_H6;
int32_t t_fine;


int32_t comptemp(int32_t temp)
{
	int32_t var1,var2,T;
	int32_t x1,x2,x3,x4,x5;
	/*x1=temp>>3;
	x2=dig_T1;
	x2<<=1;
	x3=dig_T2;
	x4=x1-x2;
	x5=x4*x3;
	x5>>=11;*/


	var1=((((temp>>3)-((int32_t)dig_T1<<1)))*((int32_t)dig_T2))>>11;
	var2=(((((temp>>4)-((int32_t)dig_T1))*((temp>>4)-((int32_t)dig_T1)))>>12)*((int32_t)dig_T3))>>14;
	t_fine=var1+var2;
	T=(t_fine*5+128)>>8;
	if(T>8500)
		T=8500;
	if(T<-4000)
		T=-4000;
	return T;
}

uint32_t comppres(int32_t adc_P)
{
	int32_t var1, var2;
	uint32_t p;
	var1 = (((int32_t)t_fine)>>1) - (int32_t)64000;
	var2 = (((var1>>2) * (var1>>2)) >> 11 ) * ((int32_t)dig_P6);
	var2 = var2 + ((var1*((int32_t)dig_P5))<<1);
	var2 = (var2>>2)+(((int32_t)dig_P4)<<16);
	var1 = (((dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((int32_t)dig_P2) * var1)>>1))>>18;
	var1 =((((32768+var1))*((int32_t)dig_P1))>>15);
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	p = (((uint32_t)(((int32_t)1048576)-adc_P)-(var2>>12)))*3125;
	if (p < 0x80000000)
	{
		p = (p << 1) / ((uint32_t)var1);
	}
	else
	{
		p = (p / (uint32_t)var1) * 2;
	}
	var1 = (((int32_t)dig_P9) * ((int32_t)(((p>>3) * (p>>3))>>13)))>>12;
	var2 = (((int32_t)(p>>2)) * ((int32_t)dig_P8))>>13;
	p = (uint32_t)((int32_t)p + ((var1 + var2 + dig_P7) >> 4));
	return p;
}

uint32_t comphum(int32_t adc_H)
{
	int32_t v_x1_u32r;
	v_x1_u32r = (t_fine - ((int32_t)76800));
	v_x1_u32r = (((((adc_H << 14) - (((int32_t)dig_H4) << 20) - (((int32_t)dig_H5) * v_x1_u32r)) +
			((int32_t)16384)) >> 15) * (((((((v_x1_u32r * ((int32_t)dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) *((int32_t)dig_H2) + 8192) >> 14));
	v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((int32_t)dig_H1)) >> 4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
	return (uint32_t)(v_x1_u32r>>12);
}


/** main function, application entry point */
int main(void)
{
	//ACA PONEMOS LO DE LA UART
	union un buffer;
	int cont = 0;
	uint8_t enviar [5];
	uint8_t auxiliar;
	int index=0;

	//memRead(BME280_I2C_ADDR_PRIM,&periodos,1);
	uint8_t info[8],dato;
	uint32_t temp=0,pres=0,hum=0,aux=0,a=0;
	int32_t tempfinal=0,presfinal=0,humfinal=0;
	uint8_t id=0,reg,i;

	dato=0XD0;

	initHardware();

	while(id!=0x60)
		regRead(SLAVE_ADD0, 0xD0, &id, 1);

	dato=0xAA;//0X25
	regWrite(SLAVE_ADD0, 0xF4, &dato, 1);
	dato=0x01;
	regWrite(SLAVE_ADD0, 0xF2, &dato, 1);
	pausems(40);

	do{regRead(SLAVE_ADD0, 0xF3, &dato, 1);
	}while(dato&0x08);
	dato=0x30;//0x10
	regWrite(SLAVE_ADD0, 0xF5, &dato, 1);
	dato=0xAB;
	regWrite(SLAVE_ADD0, 0xF4, &dato, 1);
	dato=0x01;
	regWrite(SLAVE_ADD0, 0xF2, &dato, 1);

	dato=0xB6;
	regWrite(SLAVE_ADD0, 0xE0, &dato, 1);
	//reg=0xF2;
	dato=0x05;//0x05
	regWrite(SLAVE_ADD0, 0xF2, &dato, 1); //Configurar ctrl_hum
	//reg=0xF4;
	dato=0x27;//0xAB
	regWrite(SLAVE_ADD0, 0xF4, &dato, 1); //Configurar ctrl_meas

	regRead(SLAVE_ADD0, 0xF5, &dato, 1);
	dato=0xB0;//0x00    0xA0
	regWrite(SLAVE_ADD0, 0xF5, &dato, 1);

	do{regRead(SLAVE_ADD0, 0xF3, &dato, 1);
	}while(dato&0x08);

	regRead(SLAVE_ADD0, 0x88, &dig_T1, 2);
	regRead(SLAVE_ADD0, 0x8A, &dig_T2, 2);
	regRead(SLAVE_ADD0, 0x8C, &dig_T3, 2); //Cargar valores de calibración de temperatura

	//	dig_T1=(dig_T1<<8)|((dig_T1&0xFF00)>>8);
	//	dig_T2=(dig_T2<<8)|((dig_T2&0xFF00)>>8);
	//	dig_T3=(dig_T3<<8)|((dig_T3&0xFF00)>>8);

	regRead(SLAVE_ADD0, 0x8E, &dig_P1, 2);
	regRead(SLAVE_ADD0, 0x90, &dig_P2, 2);
	regRead(SLAVE_ADD0, 0x92, &dig_P3, 2);
	regRead(SLAVE_ADD0, 0x94, &dig_P4, 2);
	regRead(SLAVE_ADD0, 0x96, &dig_P5, 2);
	regRead(SLAVE_ADD0, 0x98, &dig_P6, 2);
	regRead(SLAVE_ADD0, 0x9A, &dig_P7, 2);
	regRead(SLAVE_ADD0, 0x9C, &dig_P8, 2);
	regRead(SLAVE_ADD0, 0x9E, &dig_P9, 2); //Cargar valores de calibración de presión

	regRead(SLAVE_ADD0, 0xA1, &dig_H1, 1);
	regRead(SLAVE_ADD0, 0xE1, &dig_H2, 2);
	regRead(SLAVE_ADD0, 0xE3, &dig_H3, 1);
	regRead(SLAVE_ADD0, 0xE4, &dig_H4, 2);
	regRead(SLAVE_ADD0, 0xE5, &dig_H5, 2);
	regRead(SLAVE_ADD0, 0xE7, &dig_H6, 1); //Cargar valores de calibración de humedad
	/*dig_H4=(dig_H4&0xFF0F);
	dig_H4=(((dig_H4)|((dig_H4&0x000F)<<4))>>4);*/

	uint8_t ht4t = dig_H4 & 0xFF;
	uint8_t ht4b = (dig_H4 & 0xF00) >> 8;

	dig_H4 = ((int16_t)ht4t << 4) | ht4b;

	//dig_H5=((dig_H5&0xF000)>>4)|dig_H5;
	dig_H5=(dig_H5>>4)|((dig_H4&0xFF)>>4);

	while (1) {

		//reg=0xFA;
		//for(short int i=0;i<10;i++)
		//{
		temp=0;
		pres=0;
		hum=0;
		//aux=0;
		pausems(70);
		/*do{regRead(SLAVE_ADD0, 0xF3, &dato, 1);
		}while(dato&0x08);*/
		regRead(SLAVE_ADD0, 0xF7, info, 8);

		//aux=((0x000000FF|temp)<<16)|(((0x00F00000)|temp)>>16);
		//temp=temp&(0x0000FF00);
		//temp=temp|aux;
		//temp=info[3]|(info[4]>>8)|(info[5]>>16);
		pres=(info[0]<<12)|(info[1]<<4)|(info[2]>>4);

		//temp=(info[3]<<12)|(info[4]<<4)|(info[5]>>4);
		temp = 0;
		temp = info[3];
		temp <<= 8;
		temp |= info[4];
		temp <<= 4;
		temp |= info[5]>>4;


		hum=(info[6]<<8)|(info[7]);

		tempfinal=comptemp(temp);
		buffer.sensor=tempfinal/100;
		presfinal=comppres(pres);
		humfinal=(comphum(hum)/1024);
		buffer.sensor =presfinal/100;
		/*enviar[0]=0xaa;
		enviar[1]=buffer.separados[0];
		enviar[2]=buffer.separados[1];
		enviar[3]=0xff;
		enviar[4]=0xff;*/
		enviar[0]=0xaa;
		enviar[1]=(tempfinal/100);
		enviar[2]=humfinal;
		enviar[3]=buffer.separados[1];
		enviar[4]=buffer.separados[0];

	//	enviar[2]=buffer.separados[1];
		//enviar[3]=0xff;
//		enviar[4]=0xff;

		while (1) {
			sent_bytes = Chip_UART_SendBlocking(LPC_USART3, enviar+index,1);
			cont ++;
			/* ... */
			if (cont==PACKAGE_SZ) {
				cont=0;
				index++;
				if (index==5) {
					index=0;
					break;
				}
				auxiliar=*(enviar+index);

			}
		}
		pausems(10);




		//a=a+tempfinal;
		//}
		/*a=a/100;
		a=0;*/
	}

}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
